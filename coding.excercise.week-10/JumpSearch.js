/* Write your function for implementing the Jump Search algorithm here */
function jumpSearchTest(arr, val){
  var length = arr.length;
  var step = Math.floor(Math.sqrt(length));
  var index = Math.min(step, length)-1;
  var lb = 0;
  while (arr[Math.min(step, length)-1] < val)
  {
    lb = step;
    step += step;
    if (lb >= length){
      return -1;
    }
  }
   
  var ub = Math.min(step, length);
  while (arr[lb] < val)
  {
    lb++;
    if (lb == ub){
      return -1;
    }
  }
  if (arr[lb] == val){
     return lb;
  }
  return -1;
}

var arr = [10,20,30,40,50];


console.log("Element found at position :" + jumpSearchTest(arr, 10));
if(jumpSearchTest(arr, 26)==-1)
console.log("Element not found ");
else
console.log("Element found at position :" + jumpSearchTest(arr, 50));
console.log("Element found at position :" + jumpSearchTest(arr, 50));