let year;
let yearNot2015Or2016;

year = 2016;
//the issue here was that even if the year was 2015 or 2016
//the boolean was resulting to true which is not desired
//this was happening because the OR operator, using AND operator
//solves the problem
yearNot2015Or2016 = year !== 2015 && year !== 2016;

console.log(yearNot2015Or2016);