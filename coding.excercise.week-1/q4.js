//two numbers
var number1, number2;
//getting a random number1
number1 = Math.floor((Math.random() * 10) + 1);
//getting a random number2
number2 = Math.floor((Math.random() * 10) + 1);
//printing the number before swapping
console.log("number1 = "+number1+" number2 = "+number2);
//swapping the numbers
//storing the first number in a variable temp
var temp = number1;
//storing number2 in number1
number1 = number2;
//storing temp which stored the original number1 to number2
number2 = temp;
//printing the number after swapping
console.log("number1 = "+number1+" number2 = "+number2);