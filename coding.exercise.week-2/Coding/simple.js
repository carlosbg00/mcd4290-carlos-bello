//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    var data = [54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,-65,-14,-19,-51,-17,-25]
// take two different arrays
var pos_odd = []
var neg_even = []

// loop in through the data array
for (var i=0;i<data.length;i++){
    // check of the positive odd and negative even conditions
    if( data[i]>0 && data[i]%2){
        pos_odd.push(data[i])
    }
    else if(data[i]<0 && data[i]%2==0){
        neg_even.push(data[i])
    }
}
// display the resultant arrays
output+="Positive Odd: "+pos_odd
output+="Negative Even: "+neg_even

    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    
    //Question 2 here 
    // take variables to store count
var one = 0
var two = 0
var three = 0
var four = 0
var five = 0
var six = 0
// run a loop and call the random function
for (var i=0;i<60000;i++){

    var dice = Math.floor(Math.random() * 6) + 1

    if (dice==1){
        one+=1
    }
    else if(dice==2){
        two+=1
    }
    else if(dice==3){
        three+=1
    }
    else if(dice==4){
        four+=1
    }
    else if(dice==5){
        five+=1
    }
    else if(dice==6){
        six+=1
    }
}
// display the frequency
output+="Frequency of die rolls"
output+="1: "+one
output+="2: "+two
output+="3: "+three
output+="4: "+four
output+="5: "+five
output+="6: "+six
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}

function question3(){
    let output = "" 
    
    //Question 3 here 
    var diceNum = new Array(6).fill(0);
var randNum;

for(var i=1; i<=60000; i++){
    randNum = Math.floor(Math.random()*6) + 1;
    diceNum[randNum-1]++;
}

console.log("Frequency of dice rolls :");
for(var i=1; i<=6; i++){
    output+=i + " : " + diceNum[i-1]
}
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}

function question4(){
    let output = "" 
    
    //Question 4 here 
    var dieRolls = {    //Declaring the object
    Frequencies: {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0
    },
    Total: 60000,
    Exceptions: ""
}

for(let i=0;i<dieRolls.Total;i++){                                      //Iterating Total number of count times
    dieRolls.Frequencies[Math.floor((Math.random() * 6) + 1)]++;        // Increment the frequency of generated number
}

expectedValue = dieRolls.Total / 6;                                     // Expected value should be total / 6
onePercent = expectedValue / 100;                                       // 1% of expectedValue

for(let i=1;i<=6;i++){
    if(Math.abs(dieRolls.Frequencies[i]-expectedValue) > onePercent)   // If Absolute difference is greater than onePercent
        dieRolls.Exceptions += i + ",";                                 // This should be included in the exception string
}
dieRolls.Exceptions = dieRolls.Exceptions.substring(0,dieRolls.Exceptions.length-1); // Remove extra delimiter
// Printing all the values
output+="Frequency of dice rolls";
output+="Total rolls: ",dieRolls.Total;
output+="Frequencies: ",dieRolls.Frequencies;
output+="Exceptions: ",dieRolls.Exceptions;
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerText = output 
}

function question5(){
    let output = "" 
    
    //Question 5 here 
    //defining function 
function calculate(p) {
    let tax;
    let income = p.income;
    if (income <= 18200) {
        tax = 0;
    }
    else if (income <= 37000) {
        tax = (income - 18200) * 0.19;
    }
    else if (income <= 90000) {
        tax = (income - 37000) * 0.325 + 3572;
    }
    else if (income <= 180000) {
        tax = (income - 90000) * 0.37 + 20797;
    }
    else if (income >= 180001) {
        tax = (income - 180000) * 0.45 + 54097;
    }
    output+="%s's income is : $%f, and her tax owed is: $%f", person.name, person.income, tax.toFixed(2);
}

let person = {
    name:'Jane',
    income:127055.58
};
// calling function
calculate(person);
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}