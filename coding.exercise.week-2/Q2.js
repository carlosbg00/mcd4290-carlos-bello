// take variables to store count
var one = 0
var two = 0
var three = 0
var four = 0
var five = 0
var six = 0
// run a loop and call the random function
for (var i=0;i<60000;i++){

    var dice = Math.floor(Math.random() * 6) + 1

    if (dice==1){
        one+=1
    }
    else if(dice==2){
        two+=1
    }
    else if(dice==3){
        three+=1
    }
    else if(dice==4){
        four+=1
    }
    else if(dice==5){
        five+=1
    }
    else if(dice==6){
        six+=1
    }
}
// display the frequency
console.log("Frequency of die rolls")
console.log("1: "+one)
console.log("2: "+two)
console.log("3: "+three)
console.log("4: "+four)
console.log("5: "+five)
console.log("6: "+six)