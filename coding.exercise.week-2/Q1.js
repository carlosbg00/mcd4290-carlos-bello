var data = [54,-16,80,55,-74,73,26,5,-34,-73,19,63,-55,-61,-65,-14,-19,-51,-17,-25]
// take two different arrays
var pos_odd = []
var neg_even = []

// loop in through the data array
for (var i=0;i<data.length;i++){
    // check of the positive odd and negative even conditions
    if( data[i]>0 && data[i]%2){
        pos_odd.push(data[i])
    }
    else if(data[i]<0 && data[i]%2==0){
        neg_even.push(data[i])
    }
}
// display the resultant arrays
console.log("Positive Odd: ",pos_odd)
console.log("Negative Even: ",neg_even)
